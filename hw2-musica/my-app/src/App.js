import React, {Component} from 'react';
import Header from './components/Header/Header';
import Products from './components/Products/Products';

class App extends Component{

    state = {
        modal:false,
        basket: localStorage.getItem('basket')  ? JSON.parse(localStorage.getItem('basket')) : [],
        favourite: localStorage.getItem('favourite') ? JSON.parse(localStorage.getItem('favourite')) : []
    }

    addToBasket=(id)=>{
        this.setState({
            ...this.state,
            basket: [...this.state.basket, id]
        })
        localStorage.setItem('basket', JSON.stringify([...this.state.basket, id]))
    }

    addToFavourite = (id) => {
        this.setState({
            ...this.state,
            favourite: [...this.state.favourite, id]
        })
        localStorage.setItem('favourite', JSON.stringify([...this.state.favourite, id]))
    }
    removeFromFavourite = (id) => {
        this.setState({
            ...this.state,
            favourite: this.state.favourite.filter((favourite) => favourite !== id)
        })
        localStorage.setItem('favourite', JSON.stringify(this.state.favourite.filter((favourite) => favourite !== id)))


    }
    render(){
        return(
            <div className="App">
                <Header countBasket={this.state.basket.length} countFavourite={this.state.favourite.length}/>
                <Products addToBasket={this.addToBasket}
                          basket ={this.state.basket}
                          favourite={this.state.favourite}
                          addToFavourite = {this.addToFavourite}
                          removeFromFavourite = {this.removeFromFavourite}
                />
            </div>
        )
    }
}
export default App;

