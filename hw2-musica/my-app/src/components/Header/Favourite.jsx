import React, {Component} from "react";


class Favourite extends Component {
    render(){
        const {countFavourite} = this.props
        return (
            <div className='favourite-wrapper'>
                <img
                    className="favourite-img"
                    src='/icons/heart-329.svg'
                />
                <p className='favourite-count'>{countFavourite}</p>
            </div>
        )
    }
}

export default Favourite;