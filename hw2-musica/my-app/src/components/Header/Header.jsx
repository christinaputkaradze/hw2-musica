import React, {Component} from "react";
import Favourite from "./Favourite";
import Basket from "./Basket";
import '../Header/header.scss'

class Header extends Component{
    render(){
        const {countFavourite, countBasket} = this.props
        return(
            <header className='header-wrapper'>
                <div className='header'>
                    <div className='header-icons'>
                        <Favourite countFavourite={countFavourite}/>
                        <Basket countBasket={countBasket}/>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header;
