import React,{Component} from "react";


class Basket extends Component {
    render() {
        const {countBasket} = this.props
        return(
            <div className='basket-wrapper'>
                <img
                    className="basket-img"
                    src='/icons/shopping-cart-3041.svg'
                />
                <p className='basket-count'>{countBasket}</p>
            </div>
        )
    }
}

export default Basket;