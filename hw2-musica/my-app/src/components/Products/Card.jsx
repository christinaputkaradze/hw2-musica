import React, {Component} from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import '../Products/card.scss'


class Card extends Component {
    state = {
        favourite: false,
        modal: false,
    };

    showModal = () => {
        this.setState({
            modal: true,
        });
    };
    closeModal = () => {
        this.setState({
            modal: false,
        });
    };

    addToFavourite = () => {
        if (this.props.isCardInFavourite) {
            this.props.removeFromFavourite(this.props.product.id);
        } else {
            this.props.addToFavourite(this.props.product.id);
        }
    };
    addToBasket = () => {
            this.props.addToBasket(this.props.product.id);
            this.closeModal();
    };
    render() {
        const { art, url, name, color, price } = this.props.product;

        const actionsModal = (
            <div className="modal-footer">
                <Button
                    text="Так"
                    onClick={this.addToBasket} //then onClick={(e) => this.closeModal(e)}
                    className="modal-button"
                />
                <Button
                    text="Ні"
                    onClick={this.closeModal}
                    className="modal-button"
                />
            </div>
        );

        return (
            <div className="card">
                <div className="card-img-wrapper">
                    <img className="card-img" src={url} alt="img" />
                </div>
                <div className="card-content">
                    <p className="card-title">{name}</p>
                    <p className="card-articul">{art}</p>
                    <p className="card-color">{color}</p>
                    <div className="price-wrapper">
                        <p className="card-price">{price}</p>
                        <div
                            className="favourite-icon-wrapper"
                            onClick={this.addToFavourite}
                        >
                            <img
                                className="favourite-icon"
                                src={
                                    this.props.isCardInFavourite
                                        ? "/icons/heart-329.svg"
                                        : "/icons/heart-492.svg"
                                }
                                alt="heart"
                            />
                        </div>
                    </div>
                </div>
                <div className="card-footer">
                    <Button
                        className="card-button"
                        onClick={(e) => this.showModal(e)}
                        text="додати в кошик"
                    />
                    {this.state.modal && (
                        <Modal
                            closeButton={true}
                            header="Додати товар до кошика?"
                            actions={actionsModal}
                            onClick={(e) => this.closeModal(e)}
                        />
                    )}
                </div>
            </div>
        );
    }
}
export default Card;
