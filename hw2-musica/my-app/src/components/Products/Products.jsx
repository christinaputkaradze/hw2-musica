import React,{Component} from "react";
import '../Products/products.scss'

import Card from "./Card";

class Products extends Component{
    state = {
        products: []
    }
    componentDidMount(){

        fetch('/products.json')
            .then(response => response.json())
            .then(products =>{

                this.setState({
                    products: products,
                })

            })
    }
    render(){
        const {products} = this.state
        const product = products.map(card =>{
            const isCardInBasket = this.props.basket.includes(card.id)
            const isCardInFavourite = this.props.favourite.includes(card.id)
            return <Card
                addToBasket={this.props.addToBasket}
                removeFromBasket = {this.props.removeFromBasket}
                basket={this.props.basket}
                product ={card}
                isCardInBasket = {isCardInBasket}
                favourite= {this.props.favourite}
                addToFavourite = {this.props.addToFavourite}
                removeFromFavourite = {this.props.removeFromFavourite}
                isCardInFavourite = {isCardInFavourite}
            />
        })

        return(
            <div className="products">
                <div className="products-container">
                    <div className="products">
                        {product}
                    </div>

                </div>


            </div>
        )
    }
}
export default Products;
