import React, {Component} from "react";
import "./style.scss";

class Modal extends Component {
    constructor(props) {
        super(props);

        this.modalRef = React.createRef();
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleOutsideClick);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleOutsideClick);
    }

    handleOutsideClick(event) {
        if (this.modalRef.current && !this.modalRef.current.contains(event.target)) {
            this.props.onClick();
        }
    }

    render() {
        const { header, closeButton, actions, onClick } = this.props;
        return (
            <>
                <div className="modal-wrapper">
                    <div ref={this.modalRef} className="modal-container">
                        <div className="modal">
                            <div className="modal-header">
                                <h2 className="modal-title">{header}</h2>
                                {closeButton && <button
                                    className={'modal-close'}
                                    onClick={onClick}>
                                  Х
                                 </button>}
                            </div>
                            <div className="modal-content">
                                <div className="modal-btns">{actions}</div>
                            </div>
                        </div>
                     </div>
                </div>
            </>

        );
    }
}

export default Modal;


