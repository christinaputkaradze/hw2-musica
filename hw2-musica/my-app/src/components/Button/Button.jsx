import React, {Component} from "react";
import "./style.scss"

 class Button extends Component {
    render() {
        const { backgroundColor, text, onClick, className, type } = this.props;
        return (
            <button
                style={{ backgroundColor: backgroundColor }}
                className={className}
                onClick={onClick}
                type={type}
            >
                {text}
            </button>

        );
    }
}

Button.defaulyProps = {
    type: 'button',
}
export default Button;



